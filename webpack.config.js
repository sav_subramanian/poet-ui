const path = require('path');
const webpack = require('webpack');

module.exports = {
    devtool: 'source-map',
    devServer: {
        historyApiFallback: true
    },
    entry:{
        index:['./src/js/Root.js'],
        vendor:["jquery","tether","bootstrap","react","react-dom","react-router-dom"]
    },
    output:{
        filename: "[name].js",
        path: path.resolve(__dirname,'build'),
        publicPath: "/build"
    },
    module: {
        rules:[
            {
                use:["babel-loader"],
                test:/\.js$/,
                exclude:/node_modules/
            },
            {
                use:["style-loader","css-loader","sass-loader"],
                test:/\.scss$/,
                exclude:/node_modules/
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $:'jquery',
            jQuery:'jquery',
            Tether:'tether'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name:"vendor"
        })

        // Production build
        // new webpack.DefinePlugin({
        //     'process.env': {
        //         NODE_ENV: JSON.stringify('production')
        //     }
        // }),
        // Compress file size
        // new webpack.optimize.UglifyJsPlugin()
    ]
};
