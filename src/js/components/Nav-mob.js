import React, {Component} from 'react';
import $ from 'jquery';
import '../jquery.mmenu.all';
import LogoSVG from './svg/Logo-svg';
import DashSVG from './svg/Dashboard-svg';
import DeviceMgntSVG from './svg/Device-management-svg';
import SysAdminSVG from './svg/SystemAdmin-svg';
import AlertsSVG from './svg/Alerts-svg';
import CarotIcoSVG from './svg/Carot-ico-svg';
import CloseBtnSVG from './svg/Close-btn-svg';
import CollapseArrSVG from './svg/Collapse-arr-svg';

class NavMainMob extends Component {
    componentDidMount(){
        $(document).ready(() => {

            // Menu nav toggle state
            $('.card').on('click', function(){
                $('.card').removeClass('active');
                $(this).addClass('active');
            });

            $('.sub-nav li a').on('click', function(){
                $('.sub-nav li a').removeClass('active');
                $(this).addClass('active');
                // if($('#accordion-nav-mob .card.active')){
                //     $('#accordion-nav-mob .sub-content.collapse').addClass('show');
                // }else{
                //     $('#accordion-nav-mob .sub-content.collapse').removeClass('show');
                // }
            });

        });

        // Window resize function
        // $(window).resize(function(){
        //     // console.log('resize called');
        //     let width = $(window).width();
        //     if(width < 1200){
        //         $('#menu-main-desk').remove();
        //     }
        // }).resize();//trigger the resize event on page load.

        // Menu slide close function
        // $("#menu-main-mob").mmenu({
        //     "slidingSubmenus": false,
        //     "extensions": [
        //         "pagedim-black"
        //     ]
        // });
        // const API = $("#menu-main-mob").data( "mmenu" );
        // $("#mm-slideout-close").click(function() {
        //     API.close();
        // });

    }
    closeNavMenu(){
        document.getElementById("mySidenav-menu").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        document.getElementById("main").style.position = "inherit";
        document.getElementById("dashboard-grp").style.overflow = "none";
        // document.getElementById("dashboard-grp").style.height = "inherit";
        // document.body.style.backgroundColor = "white";
    }
    render(){
        return(
            <div id="side-navbar" className="side-navbar">
                <LogoSVG/>
                <div className="hidden-xl-up">
                    <a href="javascript:;" className="closebtn" onClick={this.closeNavMenu.bind(this)}>
                    <CloseBtnSVG/>
                    </a>
                </div>
                <div id="accordion-nav-mob" role="tablist" aria-multiselectable="true" className="main-nav">
                    <div className="card active">
                        <div className="card-header" role="tab" id="headingOne">
                            <a data-toggle="collapse" data-parent="#accordion-nav-mob" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                <DashSVG/>
                                <span>Dashboard</span>
                                <CarotIcoSVG/>
                            </a>
                        </div>

                        <div id="collapseOne" className="collapse show sub-content" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false">
                            <div className="card-block">
                                <ul className="sub-nav">
                                    <li>
                                        <a href="javascript:;" className="active">View all customers</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">View company</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">View country</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" role="tab" id="headingTwo">
                            <a data-toggle="collapse" data-parent="#accordion-nav-mob" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <DeviceMgntSVG/>
                                <span>Device management</span>
                            </a>
                        </div>
                        <div id="collapseTwo" className="collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                            <div className="card-block"></div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" role="tab" id="headingThree">
                            <a data-toggle="collapse" data-parent="#accordion-nav-mob" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                <SysAdminSVG/>
                                <span>System Admin</span>
                            </a>
                        </div>
                        <div id="collapseThree" className="collapse show" role="tabpanel" aria-labelledby="headingThree" aria-expanded="true">
                            <div className="card-block"></div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-header" role="tab" id="headingFour">
                            <a data-toggle="collapse" data-parent="#accordion-nav-mob" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                <AlertsSVG/>
                                <span>Alerts</span>
                            </a>
                        </div>
                        <div id="collapseFour" className="collapse show" role="tabpanel" aria-labelledby="headingFour" aria-expanded="true">
                            <div className="card-block"></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default NavMainMob;