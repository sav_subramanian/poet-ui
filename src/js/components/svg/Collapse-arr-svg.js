import React from 'react';

const CollapseArrSVG = () => {
    return(
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17.88 16"><path d="M17.88,1.88,16,0,8,8l8,8,1.88-1.88L11.773,8,17.88,1.88m-8,0L8,0,0,8l8,8,1.88-1.88L3.773,8Z" fill="#d5d5d5"/></svg>
    )
};

export default CollapseArrSVG;