import React from 'react';

const CloseBtnSVG = () => {
    return(
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14.5 16"><title>icon_nav_close</title><path d="M14.5,0.6L13.9,0L7.2,7.4L0.6,0L0,0.6L6.7,8L0,15.4L0.6,16l6.7-7.4l6.7,7.4l0.6-0.6L7.8,8L14.5,0.6z" fill="#d5d5d5"/></svg>
    )
};

export default CloseBtnSVG;