import React from 'react';

const CarotIcoSVG = () => {
    return(
        <svg className="carrot float-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 9.17001 6"> <path d="M7.75,6,9.17,4.59,4.58,0,0,4.59,1.41,6,4.58,2.83" fill="#d5d5d5"/></svg>
    )
};

export default CarotIcoSVG;