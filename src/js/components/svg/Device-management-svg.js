import React from 'react';

const DeviceMgntSVG = () => {
    return(
        <svg className="device" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 18.995"><path d="M2.988,3.032H16.971V16.02H2.988Z" fill="none" stroke="#d5d5d5" strokeWidth="2"/><path d="M7.006,6.991H13v5H7.006Z" fill="none" stroke="#d5d5d5" strokeWidth="2"/><path d="M10.988,0h2V2.995h-2Z" fill="#d5d5d5"/><path d="M10.988,16h2v2.995h-2Z" fill="#d5d5d5"/><path d="M6.988,0h2V2.995h-2Z" fill="#d5d5d5"/><path d="M6.988,16h2v2.995h-2Z" fill="#d5d5d5"/><path d="M0,10.032H3.04v1.984H0Z" fill="#d5d5d5"/><path d="M0,6.031H3.04V8.016H0Z" fill="#d5d5d5"/><path d="M17,10.032h3v1.984H17Z" fill="#d5d5d5"/><path d="M17,6.031h3V8.016H17Z" fill="#d5d5d5"/></svg>
    )
};

export default DeviceMgntSVG;