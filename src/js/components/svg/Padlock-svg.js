import React from 'react';

const PadlockSVG = () => {
    return(
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 21"><path
            d="M14,7H13V5A5,5,0,0,0,3,5V7H2A2.00588,2.00588,0,0,0,0,9V19a2.00588,2.00588,0,0,0,2,2H14a2.00588,2.00588,0,0,0,2-2V9A2.00588,2.00588,0,0,0,14,7ZM8,16a2,2,0,1,1,2-2A2.00588,2.00588,0,0,1,8,16Zm3.1-9H4.9V5a3.1,3.1,0,0,1,6.2,0Z" fill="#d5d5d5"/></svg>
    )
};

export default PadlockSVG;