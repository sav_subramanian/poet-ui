import React from 'react';

const AlertsSVG = () => {
    return(
        <svg className="alerts" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.5 18.5"><path d="M8.325,12.025h1.85v1.85H8.325Zm0-7.4h1.85v5.55H8.325ZM9.24075,0A9.25,9.25,0,1,0,18.5,9.25,9.24548,9.24548,0,0,0,9.24075,0ZM9.25,16.65a7.4,7.4,0,1,1,7.4-7.4A7.398,7.398,0,0,1,9.25,16.65Z" fill="#d5d5d5"/></svg>
    )
};

export default AlertsSVG;