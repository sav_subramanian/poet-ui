import React from 'react';

const DashSVG = () => {
    return(
        <svg className="dash" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 19"><path d="M20,15.039V2.004H2V15.039L20,15M20,0a2,2,0,0,1,2,2V15a2,2,0,0,1-2,2H15v2H7V17H2a2,2,0,0,1-2-2V2A2,2,0,0,1,2,0L20,.08M4,4.04h6V8.996H4V4.522M4,11h6v2H4V10.522m8-6.478h6V7H12V4.522M12,9h6v4H12Z" fill="#d5d5d5"/></svg>
    )
};

export default DashSVG;