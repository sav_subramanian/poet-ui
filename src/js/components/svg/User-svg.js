import React from 'react';

const UserSVG = () => {
    return(
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16"> <path d="M8,8A4,4,0,1,0,4,4,3.9989,3.9989,0,0,0,8,8Zm0,2c-2.67,0-8,1.34-8,4v2H16V14C16,11.34,10.67,10,8,10Z" fill="#d5d5d5"/></svg>
    )
};

export default UserSVG;