import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom';
import $ from 'jquery';
import '../jquery.mmenu.all';
import AvatarSVG from './svg/Avatar-svg';
import UserSVG from './svg/User-svg';
import SettingsSVG from './svg/Settings-svg';
import PadlockSVG from './svg/Padlock-svg';
import HelpSVG from './svg/Help-svg';
import SignoutSVG from './svg/Signout-svg';
import CloseBtnSVG from './svg/Close-btn-svg';

class NavProfileMob extends Component {
    componentDidMount(){
        $(document).ready(() => {

            // Menu nav toggle state
            $('.card').on('click', function(){
                $('.card').removeClass('active');
                $(this).addClass('active');
            });

            $('.sub-nav li a').on('click', function(){
                $('.sub-nav li a').removeClass('active');
                $(this).addClass('active');
            });

        });
    }
    closeNavProfile(){
        document.getElementById("mySidenav-profile").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
        document.getElementById("main").style.position = "inherit";
        document.getElementById("dashboard-grp").style.overflow = "none";
    }
    render(){
        return(
            <div className="side-navbar">
                <div className="hidden-xl-up">
                    <a href="javascript:;" className="closebtn" onClick={this.closeNavProfile.bind(this)}>
                    <CloseBtnSVG/>
                    </a>
                </div>
                <div id="profile-info">
                    <AvatarSVG />
                    <p className="user-name">Mohan Jesudason</p>
                    <p className="user-type">Adminstrator</p>
                </div>
                <hr className="split"/>
                <div id="accordion-settings-mob" role="tablist" aria-multiselectable="true" className="main-nav">
                    <div className="card">
                        <div className="card-header" role="tab" id="headingTwo">
                            <a data-toggle="collapse" data-parent="#accordion-settings-mob" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <UserSVG/>
                                <span>View & edit profile</span>
                            </a>
                        </div>
                        <div id="collapseTwo" className="collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                            <div className="card-block"></div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" role="tab" id="headingTwo">
                            <a data-toggle="collapse" data-parent="#accordion-settings-mob" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                <SettingsSVG/>
                                <span>Manage settings</span>
                            </a>
                        </div>
                        <div id="collapseTwo" className="collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                            <div className="card-block"></div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header" role="tab" id="headingThree">
                            <a data-toggle="collapse" data-parent="#accordion-settings-mob" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                                <PadlockSVG/>
                                <span>Change password</span>
                            </a>
                        </div>
                        <div id="collapseThree" className="collapse show" role="tabpanel" aria-labelledby="headingThree" aria-expanded="true">
                            <div className="card-block"></div>
                        </div>
                    </div>

                    <div className="card">
                        <div className="card-header" role="tab" id="headingFour">
                            <a data-toggle="collapse" data-parent="#accordion-settings-mob" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                                <HelpSVG/>
                                <span>Help</span>
                            </a>
                        </div>
                        <div id="collapseFour" className="collapse show" role="tabpanel" aria-labelledby="headingFour" aria-expanded="true">
                            <div className="card-block"></div>
                        </div>
                    </div>

                    <div className="card sign-out">
                        <div className="card-header" role="tab" id="headingFour">
                            {/*<a data-toggle="collapse" data-parent="#accordion-settings-mob" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">*/}
                                {/*<SignoutSVG/>*/}
                                {/*<span>Sign out</span>*/}
                            {/*</a>*/}

                            {/*<Link to="/" data-toggle="collapse" data-parent="#accordion-settings-mob" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour"><SignoutSVG/><span>Sign out</span></Link>*/}
                            <Link to="/"><SignoutSVG/><span>Sign out</span></Link>
                        </div>
                        <div id="collapseFour" className="collapse show" role="tabpanel" aria-labelledby="headingFour" aria-expanded="true">
                            <div className="card-block"></div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default NavProfileMob;