import React, {Component} from 'react';
import {Route, Redirect, NavLink} from 'react-router-dom';
import $ from 'jquery';
import UserSVG from './svg/User-svg';
import PadlockSVG from './svg/Padlock-svg';
import LogoBarSVG from './svg/Logo-bar-svg';

var data = { "username": "poet", "password": "Poetpoet" };

class Login extends Component {
    constructor(props){

        super(props);
        this.state = {
            languageData:{},
            langSelection:false,
            selectedLang:"",
            showLogin:true,
            showDash:false
        }
    }
    componentWillMount(){
        let _this = this;
        $.ajax({
            type: "GET",
            url: "/languages/Poet_English_AU.json",
            dataType: "json",
            success: (data) => {
                console.log(data);
                _this.setState({
                    languageData:data
                });
            }
        });

        $.ajax({
            url:'http://10.10.10.185:8000/login/',
            method:'POST',
            data:data,
            success:(data)=>{
                console.log(data);
            },
            fail:(jqXHR, textStatus, errorThrown)=>{
                console.log(`${jqXHR} - ${textStatus} - ${errorThrown}`);
            }
        });
    }
    login(e){
        e.preventDefault();

        let username = $('#username').val();
        let password = $('#password').val();
        let rememberMe = document.getElementById('remember-me').checked;

        let loginData = {
            username: username,
            password: password
        };

        let data2 = JSON.stringify(loginData);

        // console.log(`${username} - ${password} - ${rememberMe}`);
        console.log(data2);

        // $.ajax({
        //     url:'http://10.10.10.185:8000/login/',
        //     method:'POST',
        //     data:{ "username": "poet", "password": "Poetpoet" },
        //     // data:{"username": "+poet+", "password": "+poet},
        //     success:(data)=>{
        //         console.log(`${data.token}`);
        //     },
        //     fail:(jqXHR, textStatus, errorThrown)=>{
        //         console.log(`${jqXHR} - ${textStatus} - ${errorThrown}`);
        //     }
        // });

    }
    render(){
        return (
            <div>
                <div className="login-screen">
                    <div className="login-bg-image">
                        {/*<img className="login-image-overlay logo img-fluid" src="/images/FS-home-logo.png" alt=""/>*/}
                        <LogoBarSVG/>
                    </div>

                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 push-sm-2 col-md-6 push-md-3">
                                <div className="login-panel">
                                    <h4>{this.state.languageData["fs.login.signIn"]}</h4>
                                    <form id="login-form">
                                        <div className="form-group">
                                            <input id="username" type="text" className="form-control" placeholder={this.state.languageData["fs.login.username"]}/>
                                            <UserSVG/>
                                        </div>
                                        <div className="form-group">
                                            <input id="password" type="password" className="form-control" placeholder={this.state.languageData["fs.login.password"]}/>
                                            <PadlockSVG/>
                                        </div>
                                        <div className="form-group check-box">
                                            <input id="remember-me" type="checkbox" className="form-check-input"/>
                                            <label className="form-check-label"></label>
                                            <span>{this.state.languageData["fs.login.rememberMe"]}</span>
                                        </div>
                                        <hr/>
                                        <div className="form-group check-box">
                                            <a href="javascript:;">{this.state.languageData["fs.login.forgot_password"]}</a>
                                            {/*<NavLink to="/dashboard" className="btn btn-primary float-right green">{this.state.languageData["fs.login.signIn"]}</NavLink>*/}
                                            <button id="submit" type="submit" className="btn btn-primary float-right green" onClick={this.login.bind(this)}>{this.state.languageData["fs.login.signIn"]}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="login-bg-image top banner-white">
                    </div>

                    <div className="login-bg-image bottom hidden-xs-down">
                    </div>

                </div>
                {/*<Route exact path="/" render={ () => <Redirect to="/"/> }/>*/}
            </div>
        )
    }
}

export default Login;