import React, {Component} from 'react';
import {Link, NavLink} from 'react-router-dom';
import $ from 'jquery';
import '../jquery.mmenu.all';
import NavDesk from './Nav';
import NavProfileMob from './Nav-profile-mob';
import UserSVG from './svg/User-svg';
import NotificationSVG from './svg/Notification-svg';
import HelpSVG from './svg/Help-svg';
import SettingsSVG from './svg/Settings-svg';
import Highcharts from 'highcharts';

// Dynamic data test
// Time conversion
function msToTime(duration) {
    let milliseconds = parseInt((duration%1000)/100)
        , seconds = parseInt((duration/1000)%60)
        , minutes = parseInt((duration/(1000*60))%60)
        , hours = parseInt((duration/(1000*60*60))%24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    // return hours + ":" + minutes + ":" + seconds + ":" + milliseconds;
    return hours + ":" + minutes + ":" + seconds;
}

let commandValue = "GET_METER_SUMMATION_DELIVERED";
let fromDate = '';
let fromTime = '';
let toDate = '';
let toTime = '';
let countSuccess = 0;
let countFail = 0;
let countNoResult = 0;
let countOtherError = 0;
let logFnGrp = [];
let gateWayLogs = [];
let appServerLogs = [];


function jsUpdateSize(){
    // Get the dimensions of the viewport
    let width = window.innerWidth;
    let height = window.innerHeight;

    document.getElementById('jsWidth').innerHTML = width;  // Display the width
    document.getElementById('jsHeight').innerHTML = height;// Display the height

    let menu = document.getElementsByClassName('sidenav');

    if(width < 1200){

        $('.menu.sidenav').removeAttr('id','menu-main-desk');
        $('.menu').attr('id','mySidenav-menu');
        $('.menu').addClass('mob-menu sidenav');
        $('.side-navbar').removeClass('nav-collapse');
    } else{
        $('.menu.sidenav').removeAttr('id','mySidenavmenu');
        $('.menu').removeClass('mob-menu sidenav');
        $('.menu').attr('id','menu-main-desk');
    }
};

window.onload = jsUpdateSize;       // When the page first loads
window.onresize = jsUpdateSize;     // When the browser changes size

function chart(element,type,categories,data){
    return Highcharts.chart(element, {
        chart: {
            type: type
        },
        title: {
            text: 'Time Responded'
        },
        subtitle: {
            text: 'GET_METER_SUMMATION_DELIVERED'
        },
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: 'Time in milliseconds'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [
            {
                name: 'NIC ID',
                data: data
            }
        ]
    });
}

const style = {
    minWidth: "310px",
    height: "400px",
    margin: "0 auto"
};

class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            eid:[],
            sessionid:null,
            response:null,
            cid:null,
            data1:null,
            data2:null,
            data3:null,
            showFailed:false,
            showPassed:false,
            showAll:true,
            preloader:false,
            loginPortal:null,
            fetchFail:false,
            AddLogs:false,
            showAppSerLogs:false,
            showAppSerLogsV:null,
            showGatewayLogs:false,
            showGatewayLogsBtn:false,
            showGatewayLogsV:null,
            data1ID:null,
        }
    }
    componentWillMount(){



        let data = [];
        let data2 = [];
        let data3 = [];
        let data1ID = [];
        let ATime1ID = [];
        countSuccess = 0;
        countFail = 0;
        countNoResult = 0;
        countOtherError = 0;

        const _this = this;

        _this.setState({
            data1:null,
            data2:null,
            data3:null,
            data1ID:null,
            ATime1ID:null,
            showGatewayLogsBtn:false,
            preloader:true
        });

        fromDate = $('#from').val();
        fromTime = $('#from-time').val();
        toDate = $('#to').val();
        toTime = $('#to-time').val();
        let newFromDate = fromDate;
        let newToDate = toDate;
        let newFromDateTime = newFromDate + 'T' + fromTime;
        let newToDateTime = newToDate + 'T' + toTime;

        $.ajax({
            type: "GET",
            ifModified: true,
            url: `http://lora-yl-admin.cloud.freestyleiot.com/getTransactions?sessionid=a0980c99a36fd74570c79ad8f44c628f&qtype=cid|command&query=1,%3E0|GET_METER_SUMMATION_DELIVERED&after=2017-05-09T12:00:00&before=2017-05-09T13:00:00`,
            dataType: "json",
            success: function (result) {
                // console.log(result);

                let resultLength = result.rows.length;
                // console.log(resultLength);

                // iterate over each element in the array
                for (let i = 0; i < resultLength; i++){


                    let dataResult = '';

                    if((result.rows[i].data === null) || (result.rows[i].data === '')){
                        dataResult = 'empty';
                    } else{

                        if(!result.rows[i].data.includes("\"result_code\"")){
                            dataResult = 'noResultCode';
                        }

                        if(result.rows[i].data.includes("\"result_code\":0")){
                            dataResult = 'success';
                        }

                        if(result.rows[i].data.includes("\"result_code\":1")){
                            dataResult = 'error';
                        }

                        if((result.rows[i].data.includes("\"result_code\":2")) || (result.rows[i].data.includes("\"result_code\":3")) || (result.rows[i].data.includes("\"result_code\":4")) || (result.rows[i].data.includes("\"result_code\":5")) || (result.rows[i].data.includes("\"result_code\":6")) || (result.rows[i].data.includes("\"result_code\":7")) || (result.rows[i].data.includes("\"result_code\":8")) || (result.rows[i].data.includes("\"result_code\":9"))){
                            dataResult = 'alert';
                        }
                    }

                    if(result.rows[i].status !== "Issued "){
                        data.push({
                            id:result.rows[i].id,
                            eid:result.rows[i].eid,
                            cid:result.rows[i].cid,
                            command:result.rows[i].command,
                            status:result.rows[i].status,
                            data:result.rows[i].data,
                            result:dataResult,
                            time:result.rows[i].time,
                            statusTS:result.rows[i].status_ts
                        });
                    } else {
                        data2.push({
                            id:result.rows[i].id,
                            eid:result.rows[i].eid,
                            cid:result.rows[i].cid,
                            command:result.rows[i].command,
                            status:result.rows[i].status,
                            data:result.rows[i].data,
                            result:dataResult,
                            time:result.rows[i].time,
                            statusTS:result.rows[i].status_ts
                        });
                    }
                }
                _this.setState({
                    data1: data,
                    data2: data2

                });
            },
            error: function (data, status, req) {
                console.log(req.responseText + " " + status);
                console.log(data);
                _this.setState({
                    preloader:false,
                    fetchFail:true
                });
            }
        });

        // $(function(){
        //     Highcharts.chart('container', {
        //         chart: {
        //             type: 'line'
        //         },
        //         title: {
        //             text: 'Monthly Average Temperature'
        //         },
        //         subtitle: {
        //             text: 'Source: WorldClimate.com'
        //         },
        //         xAxis: {
        //             categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        //         },
        //         yAxis: {
        //             title: {
        //                 text: 'Temperature (°C)'
        //             }
        //         },
        //         plotOptions: {
        //             line: {
        //                 dataLabels: {
        //                     enabled: true
        //                 },
        //                 enableMouseTracking: false
        //             }
        //         },
        //         series: [{
        //             name: 'Tokyo',
        //             data: [7.0, 6.9, 9.5, 14.5, 18.4, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6]
        //         }, {
        //             name: 'London',
        //             data: [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8]
        //         },
        //         {
        //             name: 'New York',
        //             data: [5.9, 1.2, 2.7, 5.5, 15.9, 10.2, 7.0, 2.6, 2.2, 6.3, 2.6, 1.8]
        //         }
        //         ]
        //     });
        // });

        setTimeout(function(){
            if((_this.state.data1 !== null)&&(_this.state.data2 !== null)) {
                for (let i = 0; i < _this.state.data1.length; i++) {
                    for (let j = 0; j < _this.state.data2.length; j++) {
                        if ((_this.state.data1[i].eid === _this.state.data2[j].eid) && (_this.state.data1[i].cid === _this.state.data2[j].cid)) {

                            let dataResult = '';

                            if((_this.state.data1[i].data === null) || (_this.state.data1[i].data === '')){
                                dataResult = 'empty';
                                countNoResult++;
                            } else{

                                if(!_this.state.data1[i].data.includes("\"result_code\"")){
                                    dataResult = 'noResultCode';
                                    countNoResult++;
                                }

                                if(_this.state.data1[i].data.includes("\"result_code\":0")){
                                    dataResult = 'success';
                                    countSuccess++;
                                }

                                if(_this.state.data1[i].data.includes("\"result_code\":1")){
                                    dataResult = 'error';
                                    countFail++;
                                }

                                if((_this.state.data1[i].data.includes("\"result_code\":2")) || (_this.state.data1[i].data.includes("\"result_code\":3")) || (_this.state.data1[i].data.includes("\"result_code\":4")) || (_this.state.data1[i].data.includes("\"result_code\":5")) || (_this.state.data1[i].data.includes("\"result_code\":6")) || (_this.state.data1[i].data.includes("\"result_code\":7")) || (_this.state.data1[i].data.includes("\"result_code\":8")) || (_this.state.data1[i].data.includes("\"result_code\":9"))){
                                    dataResult = 'alert';
                                    countOtherError++;
                                }
                            }
                            // console.log(dataResult);

                            let respTimeOrig = _this.state.data1[i].time;
                            let respTime = respTimeOrig.split(".");
                            respTime = respTime[0].split("T");
                            respTimeOrig = new Date(respTimeOrig);
                            let respTimeStamp = respTimeOrig;
                            // console.log(respTimeOrig);
                            let respTimeOrigMilli = respTimeOrig.getTime();
                            // console.log(respTimeOrigMilli);

                            let issuedTimeOrig = _this.state.data2[j].time;
                            let issuedTime = issuedTimeOrig.split(".");
                            issuedTime = issuedTime[0].split("T");
                            issuedTimeOrig = new Date(issuedTimeOrig);
                            // console.log(issuedTimeOrig);
                            let issuedTimeOrigMilli = issuedTimeOrig.getTime();
                            // console.log(issuedTimeOrigMilli);

                            let diffMilli = ((respTimeOrigMilli) - (issuedTimeOrigMilli));
                            // console.log(diffMilli);
                            let diffHMS = msToTime(diffMilli);
                            // console.log(diffHMS);

                            data3.push({
                                eid: _this.state.data1[i].eid,
                                cid: _this.state.data1[i].cid,
                                command:_this.state.data1[i].command,
                                // eid2: _this.state.data2[j].eid,
                                status: _this.state.data1[i].status,
                                status2: _this.state.data2[j].status,
                                // cid2: _this.state.data2[j].cid,
                                data:_this.state.data1[i].data,
                                result:dataResult,
                                // respondedTime: _this.state.data1[i].time,
                                issuedTimeStamp: _this.state.data2[j].time,
                                issuedDate: issuedTime[0],
                                issuedTime: issuedTime[1],
                                enqueuedTime: null,
                                acknowledgedTime: null,
                                acknowledgedTimeStamp: null,
                                gatewayTimeStamp:[],
                                gatewayTime:[],
                                gatewayDate:[],
                                gatewayDataType:[],
                                gatewayDataValue:[],
                                respondedTS: respTimeStamp,
                                respondedDate: respTime[0],
                                respondedTime: respTime[1],
                                awaitedTime: diffHMS
                            });
                        }
                    }
                }

                _this.setState({
                    data3:data3,
                    preloader:false
                });

                setTimeout(function(){
                    for(let i = 0; i < _this.state.data3.length; i++){
                        data1ID.push(_this.state.data3[i].eid);
                        let timeConvert = _this.state.data3[i].awaitedTime;
                        timeConvert = timeConvert.replace(/00:/g,"");
                        timeConvert = timeConvert.replace(/:/g,".");
                        timeConvert = parseFloat(timeConvert);
                        console.log(timeConvert);
                        ATime1ID.push(timeConvert);
                    }
                    _this.setState({
                        data1ID: data1ID,
                        ATime1ID: ATime1ID
                    });
                },500)
            }
        },1000);

        setTimeout(function(){
            $(function(){

                // Highcharts.chart('chart', {
                //     chart: {
                //         type: 'line'
                //     },
                //     title: {
                //         text: 'Time Responded'
                //     },
                //     subtitle: {
                //         text: 'GET_METER_SUMMATION_DELIVERED'
                //     },
                //     xAxis: {
                //         // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                //         categories: _this.state.data1ID
                //         // categories: ['18234', '14523', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                //     },
                //     yAxis: {
                //         title: {
                //             text: 'Time in milliseconds'
                //         }
                //     },
                //     plotOptions: {
                //         line: {
                //             dataLabels: {
                //                 enabled: true
                //             },
                //             enableMouseTracking: false
                //         }
                //     },
                //     series: [
                //         {
                //             name: 'NIC ID',
                //             // data: [1.09, 1.2, 2.7, 5.5, 15.9, 10.2, 7.0, 2.6, 2.2, 6.3, 2.6, 1.8]
                //             data: ATime1ID
                //         }
                //     ]
                // });

                // Highcharts.chart('chart6-1', {
                //     chart: {
                //         type: 'column'
                //     },
                //     title: {
                //         text: 'Time Responded'
                //     },
                //     subtitle: {
                //         text: 'GET_METER_SUMMATION_DELIVERED'
                //     },
                //     xAxis: {
                //         // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                //         categories: _this.state.data1ID
                //         // categories: ['18234', '14523', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                //     },
                //     yAxis: {
                //         title: {
                //             text: 'Time in milliseconds'
                //         }
                //     },
                //     plotOptions: {
                //         line: {
                //             dataLabels: {
                //                 enabled: true
                //             },
                //             enableMouseTracking: false
                //         }
                //     },
                //     series: [
                //         {
                //             name: 'NIC ID',
                //             // data: [1.09, 1.2, 2.7, 5.5, 15.9, 10.2, 7.0, 2.6, 2.2, 6.3, 2.6, 1.8]
                //             data: ATime1ID
                //         }
                //     ]
                // });
                // Highcharts.chart('chart6-2', {
                //     chart: {
                //         type: 'bar'
                //     },
                //     title: {
                //         text: 'Time Responded'
                //     },
                //     subtitle: {
                //         text: 'GET_METER_SUMMATION_DELIVERED'
                //     },
                //     xAxis: {
                //         // categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                //         categories: _this.state.data1ID
                //         // categories: ['18234', '14523', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                //     },
                //     yAxis: {
                //         title: {
                //             text: 'Time in milliseconds'
                //         }
                //     },
                //     plotOptions: {
                //         line: {
                //             dataLabels: {
                //                 enabled: true
                //             },
                //             enableMouseTracking: false
                //         }
                //     },
                //     series: [
                //         {
                //             name: 'NIC ID',
                //             // data: [1.09, 1.2, 2.7, 5.5, 15.9, 10.2, 7.0, 2.6, 2.2, 6.3, 2.6, 1.8]
                //             data: ATime1ID
                //         }
                //     ]
                // });

                chart('chart','line',_this.state.data1ID,_this.state.ATime1ID);
                chart('chart6-1','column',_this.state.data1ID,_this.state.ATime1ID);
                chart('chart6-2','bar',_this.state.data1ID,_this.state.ATime1ID);

            });
        },2000);

    }
    componentDidMount(){
        const _this = this;

        $('.arr-hide').one('click', function(){
            // alert('clicked');


            setTimeout(function(){
                $('#chart, #chart6-1, #chart6-2').innerHTML = "";
            },100);

            setTimeout(function(){
                chart('chart','line',_this.state.data1ID,_this.state.ATime1ID);
                chart('chart6-1','column',_this.state.data1ID,_this.state.ATime1ID);
                chart('chart6-2','bar',_this.state.data1ID,_this.state.ATime1ID);
            },900);


        });
    }
    openNavProfile(){
        document.getElementById("mySidenav-profile").style.width = "320px";
        document.getElementById("mySidenav-profile").style.height = "100%";
        document.getElementById("main").style.marginLeft = "-320px";
        document.getElementById("main").style.position = "fixed";
        document.getElementById("dashboard-grp").style.overflow = "hidden";
    }
    openNavMenu(){
        document.getElementById("mySidenav-menu").style.width = "320px";
        document.getElementById("mySidenav-menu").style.height = "100%";
        document.getElementById("main").style.marginLeft = "320px";
        document.getElementById("main").style.position = "fixed";
        document.getElementById("dashboard-grp").style.overflow = "hidden";
    }
    render(){

        return(
            <div id="dashboard-grp" onLoad={jsUpdateSize}>
                <div className="window-size hidden-xs-up">
                    <p id="jsWidth"></p>
                    <p id="jsHeight"></p>
                </div>
                <div className="side-nav-grp">
                    <nav id="menu-main-desk" className="menu">
                        <NavDesk />
                    </nav>
                    <nav id="mySidenav-profile" className="sidenav mob-profile">
                        <NavProfileMob />
                    </nav>
                </div>

                <div id="main" className="main-component">
                    <div className="container-fluid">
                        <div className="row hidden-lg-down">
                            <div className="col-sm-12">
                                <div className="profile-settings float-right">
                                    <NotificationSVG/>
                                    <HelpSVG/>
                                    <SettingsSVG/>
                                    <img className="profile-picture" src="/images/ICON_avatar_none.png" alt="user profile picture"/>

                                    <div className="btn-group">
                                        <span className="admin-text-grp dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin <svg className="prof-menu" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 5"><polygon points="0 5 10 5 5.001 0 0 5"/></svg></span>
                                        <div className="dropdown-menu">
                                            <a className="dropdown-item" href="javascript:;">View & edit profile</a>
                                            <a className="dropdown-item" href="javascript:;">Manage settings</a>
                                            <a className="dropdown-item" href="javascript:;">Change password</a>
                                            <a className="dropdown-item" href="javascript:;">Help</a>
                                            <div className="dropdown-divider"></div>
                                            <Link to="/" className="dropdown-item">Sign out</Link>
                                            {/*<a className="dropdown-item" href="javascript:;">Sign out</a>*/}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div className="row mob-nav-grp hidden-xl-up">
                            <div className="col-6">
                                <a id="mob-menu-btn" href="javascript:;" onClick={this.openNavMenu.bind(this)}>Menu</a>
                            </div>
                            <div className="col-6 settings">
                                <a id="mob-menu-settings-btn" href="javascript:;" onClick={this.openNavProfile.bind(this)}>
                                    <UserSVG/>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="container-fluid">
                        <div className="row">
                            <div className="chart-group-row1"></div>
                            <div id="chart6-1" className="col-sm-6 chart-grp" style={style}>

                            </div>
                            <div id="chart6-2" className="col-sm-6 chart-grp" style={style}>

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <div className="chart-group-row2"></div>
                                <div id="chart" style={style} className="chart-grp"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard;