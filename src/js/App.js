import React from 'react';
import {BrowserRouter, Route, NavLink} from 'react-router-dom';
import Login from './components/Login';
import Dashboard from './components/Dashboard';

const App = () => {
    return(
        <BrowserRouter>
            <div>
                <Route path="/dashboard" component={Dashboard}/>
                <Route exact path="/" component={Login}/>
            </div>
        </BrowserRouter>
    )
};

export default App;